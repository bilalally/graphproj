pathfinder.py is the python file for the initial problem in part one,
shortestpath.py is the python file for the proof of concept

Both projects require the networkX library, all instructions for installation:
https://networkx.github.io/documentation/stable/install.html