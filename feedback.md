# Evaluation (60/100)

## Knowledge (25/40)

+ Some relevant theoretical knowledge demonstrated (e.g. graph representations of data, Dijkstra etc)
+ Some awareness of testing strategies but what you've presented in your test table can't be called unit testing as the tests cannot be performed at the unit test level (at least not without wading into the networkx library). These tests can only be performed at the system level here...there has been no modularisation of the code to facilitate unit testing. Also, what 'general cases' are the tests you've chosen representative of? I can guess at some of them, but others are not so obvious. Include test descriptions to clarify what the purpose of each test is. We'll look at testing again as a group, as you aren't the only one to exhibit some confusion around testing.

## Critical thinking (24/40)

+ Ability to analyse a problem and select some appropriate tools evidenced
+ You've said,

> It’s worth noting the brief wanted the graph to be a user input

I don't think it mentioned 'user' at any point in Part 1. It only mentioned inputs in relation to the function it asked you to write (`has_path` is your solution to Part 1).
+ In your evaluation you claim to have satisfied all the requirements of the brief...I'm not so sure yo satisfied *all* of them. A more careful analysis of future assignment briefs is advised.

## Academic quality (11/20)

+ Report is well written
+ Limited references. Surely you referred to more than 2 sources of information? For a project of this magnitude (60% of module) I'd expect you to access a wider range of scholarly texts (maybe a book or two from the module reading list?)
+ I would avoid excessive inclusion of screenshots of code. I can see the code from the repo. If you think it's important to show an extract in the body of the text, write it in a courier font and indent it, or box it in? Screenshots of code aren't always easy to read, don't print or resize well, and can look messy if all different sizes.

# Artefact (52/100)

## Problem solving (27/40)

+ The problem posed in the brief (Part 1) was understood
+ Graphs were identified as an appropriate modelling tool
+ Good job identifying an appropriate Python package for this problem, although it took most of the hard problem-solving work out of the equation!
+ Data modelling - I wonder why you didn't make the Graph data in pathfinder.py reflect the initial problem you were given? The graph was not the same.

## Programming (15/30)

+ The code base is well organised with a README file.
+ Despite suppressing opportunity for problem-solving, using a library is sensible from a software development perspective
+ Some knowledge of Python environments and libraries evidenced
+ Better commenting and some effort to refactor the code lacking
+ There isn't a lot to look at...the code doesn't go beyond the example snippets you can find in the networkx documentation.

## Testing (6/20)

+ The test table suggests you did some system-level manual behavioural testing
+ I could see no evidence of automated unit testing in the code base. Encapulating your pathfinder code in a function and writing another function to test it with some different inputs would not have involved an awful lot of work. The user interface part of the code would then be a separate function with error handling built-in.
+ The shortestpath script appears to output the best route between 2 nodes. Again, there is no evidence of automated unit testing. Again, separating the bit of the function which handles the user input and the bit that finds the shortest path into 2 separate functions would have enabled you to write a very simple automated unit test for the shortest path bit.
+ You mention in the report that you've assumed the networkx functions are tested by the project maintainers...a fair assumption, but not one that gets you off the hook completely! If you'd done more in the way of program development, you'd have functions of your own to test. 

## Creativity (4/10)

+ The association with Dijkstra's algorithm recognised
+ According to the project brief,

> whatever you produce [for the PoC] should be sufficient to demonstrate to a non-specialist where the application for your algorithm lies

I'm not convinced your script really achieves this. Yes it demonstrates it is possible to find the shortest path between 2 nodes using an 'off the shelf' solution, but how does it show a non-specialist why/where this is useful? Something with a simple graphical user interface, or even just on-screen visuals, possibly with 'faked' functionality to demonstrate a common use case would have been more effective I think. I'm making this point in relation to creativity, as I think it requires creativity to identify applications for something that is inherently abstract. You treated Parts 1 and 3 as if they were (almost) the same in terms of their requirements.

Overall, this is a fair piece of work given that it largely achieves the goals, but you neglected to demonstrate much of the software development techniques that were highlighted on the module (testing, modularising code with functions, meaningful comments, etc.)

I suspect starting earlier and getting more clarification on the project aims while in class might be a goal for your next independent project. There is some degree of frustration around the fact that you've produced something reasonably convincing at the 11th hour...one wonders what you might achieve if you adapted your processes...
