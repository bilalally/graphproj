import networkx as nx
G=nx.empty_graph()
#first graph
G.add_nodes_from([0, 6])
G.add_edge(0,1)
G.add_edge(0,2)
G.add_edge(1,2)
G.add_edge(1,3)
G.add_edge(1,4)
G.add_edge(2,4)
G.add_edge(3,4)
G.add_edge(4,5)
G.add_edge(5,6)
#2nd graph
G.add_nodes_from([7,9])
G.add_edge(7,8)
G.add_edge(8,9)


first = int(input("Enter starting node: "))
second = int(input("Enter end node: "))

paths = nx.has_path(G,first,second)
print(paths)
