import networkx as nx
G=nx.empty_graph()
#first graph
G.add_nodes_from([0, 6])
G.add_edge(0,1,weight=0.6)
G.add_edge(0,2,weight=2)
G.add_edge(1,2,weight=3)
G.add_edge(1,3,weight=5)
G.add_edge(1,4,weight=1)
G.add_edge(2,4,weight=0.3)
G.add_edge(3,4,weight=13)
G.add_edge(4,5,weight=5)
G.add_edge(5,6,weight=1)
#2nd graph
G.add_nodes_from([7,9])
G.add_edge(7,8,weight=0.2)
G.add_edge(8,9,weight=3)


first = int(input("Enter starting node: "))
second = int(input("Enter end node: "))



paths = nx.dijkstra_path(G,first,second)
print(paths)
